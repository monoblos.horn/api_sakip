<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstrarenstra extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_renstra";

    protected $primaryKey = 'id_renstra_renstra';
    public function renstravisi()
    {
        return $this->hasMany('App\Renstravisi', 'renstra_renstra_id', 'id_renstra_renstra');
    }

    public function renstratujuan()
    {
        return $this->hasMany('App\Renstratujuan', 'renstra_renstra_id', 'id_renstra_renstra');
    }

    public function renstrasasaran()
    {
        return $this->hasMany('App\Renstrasasaran', 'renstra_renstra_id', 'id_renstra_renstra');
    }

    public function renstraprogram()
    {
        return $this->hasMany('App\Renstraprogram', 'renstra_renstra_id', 'id_renstra_renstra');
    }

    public function renstramisi()
    {
        return $this->hasMany('App\Renstramisi', 'renstra_renstra_id', 'id_renstra_renstra');
    }

    

    
    
}
