<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstravisi extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_visi";

    protected $primaryKey = 'id_renstra_visi';
}
