<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sikdsatker extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "sikd_satker";

    protected $primaryKey = 'id_sikd_satker';
    public function renstrasatker()
    {
        return $this->hasMany('App\Renstrasatker', 'sikd_satker_id', 'id_sikd_satker');
    }

}
