<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstratujuanmisi extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_tujuan_misi";

    protected $primaryKey = 'id_renstra_tujuan_misi';
}
