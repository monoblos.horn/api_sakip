<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstrasatker extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_satker";

    protected $primaryKey = 'id_renstra_satker';
    public function renstravisi()
    {
        return $this->hasMany('App\Renstravisi', 'renstra_renstra_id', 'id_renstra_satker');
    }

    public function renstramisi()
    {
        return $this->hasMany('App\Renstramisi', 'renstra_renstra_id', 'id_renstra_satker');
    }

    public function renstratujuan()
    {
        return $this->hasMany('App\Renstratujuan', 'renstra_renstra_id', 'id_renstra_satker');
    }

    public function renstrasasaran()
    {
        return $this->hasMany('App\Renstrasasaran', 'renstra_renstra_id', 'id_renstra_satker');
    }

    public function renstraprogram()
    {
        return $this->hasMany('App\Renstraprogram', 'renstra_renstra_id', 'id_renstra_satker');
    }

    

}
