<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstraprogram extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_program";

    protected $primaryKey = 'id_renstra_renstra';



}
