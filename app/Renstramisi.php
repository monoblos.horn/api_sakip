<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstramisi extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_misi";

    protected $primaryKey = 'id_renstra_misi';

    public function renstratujuanmisi()
    {
        return $this->hasMany('App\Renstratujuanmisi', 'renstra_tujuan_id', 'id_renstra_tujuan');
    }
}
