<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstrasasaran extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_sasaran";

    protected $primaryKey = 'id_renstra_sasaran';

    public function renstrasasaranprogram()
    {
        return $this->hasMany('App\Renstrasasaranprogram', 'renstra_sasaran_id', 'id_renstra_sasaran');
    }

}
