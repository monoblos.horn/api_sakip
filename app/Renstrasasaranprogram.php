<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstrasasaranprogram extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_sasaran_program";

    protected $primaryKey = 'id_renstra_sasaran_program';

}
