<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Renstrarenstra;
use App\Sikdsatker;
use App\Renstrasatker;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rpjmd()
    {
        $data = Renstrarenstra::with([  "renstravisi", 
                                        "renstramisi", 
                                        "renstratujuan","renstratujuan.renstratujuanmisi", 
                                        "renstrasasaran","renstrasasaran.renstrasasaranprogram", 
                                        "renstraprogram"])->where('id_renstra_renstra', '5161609055314050')->get();
        return $data;
    }

    public function renstra()
    {
        $data = Sikdsatker::get();
        return $data;
    }

    public function renstra_detail($id)
    {
        $data = Sikdsatker::with([  "renstrasatker", "renstrasatker.renstravisi", 
                                    "renstrasatker.renstramisi", 
                                    "renstrasatker.renstratujuan", "renstrasatker.renstratujuan.renstratujuanmisi", 
                                    "renstrasatker.renstrasasaran", "renstrasatker.renstrasasaran.renstrasasaranprogram", 
                                    "renstrasatker.renstraprogram"])->find($id);
        return $data;
    }
    
    public function list_opd()
    {
        $data = Sikdsatker::select('id_sikd_satker', 'nama as nama_opd')->get();
        return $data;
    }

    public function detail_opd($id)
    {
        $data = Sikdsatker::select('id_sikd_satker', 'nama as nama_opd')->find($id);
        return $data;
    }
    
}
