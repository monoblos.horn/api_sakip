<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Renstratujuan extends Model
{
    public $timestamps = false;
    public $incrementing = false;
    protected $table = "renstra_tujuan";

    protected $primaryKey = 'id_renstra_tujuan';

    public function renstratujuanmisi()
    {
        return $this->hasMany('App\Renstratujuanmisi', 'renstra_tujuan_id', 'id_renstra_tujuan');
    }

}


